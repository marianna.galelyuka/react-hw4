import React, { useState } from 'react';
import Button from '../button';
import { FaStar } from "react-icons/fa";
import PropTypes from 'prop-types';

import styles from "./Product.module.css";

const Product = ({ product, onAddToFavorites, openModal }) => {
    const favorites = JSON.parse(localStorage.getItem('favorites'));
    const [isFavorite, setIsFavorite] = useState(favorites.find(item => product.id === item.id));
    
    const changeFavorite = () => {
        setIsFavorite(!isFavorite);
        isFavorite ? product.isFavorite = false : product.isFavorite = true;
        onAddToFavorites(product);
    }

    return (
        <div className={styles.product} >
            <img className={styles.product__img} src={product.imgUrl} alt="Product" />
            <FaStar id={product.id} onClick={changeFavorite} className={`${styles.product__favIcon} ${isFavorite && `${styles.active}`}`}  />
            <h4 className={styles.product__title}>{product.productName}</h4>
            <div className={styles.product__details}>
                <p className={styles.product__details_color}> {product.color}</p>
                <p className={styles.product__details_price}> UAH {product.price}</p>
            </div>
            <Button id={product.id} text="Add to cart" className={styles.product__toCartBtn} onClick={() => openModal(product)} />
        </div>
    )
}

Product.propTypes = {
    isFavorite: PropTypes.bool,
    product: PropTypes.object,
    imgUrl: PropTypes.string,
    id: PropTypes.number,
    productName: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.number,
};

export default Product;