import React from 'react';
import Product from '../product';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from "react-redux";
// import { closeModalAC, openModalAC } from '../../redux/modal/modalActions';
import { toggleOpenModalAC } from '../../redux/modal/modalActions';
import Modal from '../modal';
import Button from '../button';

import styles from './ProductList.module.css';

const ProductList = ({ onAddToCart, onAddToFavorites }) => {
    const products = useSelector(state => state.products.products);
    const modal = useSelector(state => state.modal.isOpened);
    const productModal = useSelector(state => state.modal.productModal);
    const dispatch = useDispatch();

    // const openModal = (product) => {
    //     dispatch(openModalAC(product));
    // };

    // const closeModal = () => {
    //     dispatch(closeModalAC());
    // };

    const toggleOpenModal = (product) => {
        dispatch(toggleOpenModalAC(product));
    };

    const handleAddToCart = () => {
        onAddToCart(productModal);
        toggleOpenModal(productModal.id);
    }

    return (
        <main>
            <h2 className={styles.main__title}>Our products</h2>
            <div className={styles.productList}>
                {products.map((el) => (
                    <Product product={el} key={el.id} onAddToCart={onAddToCart} onAddToFavorites={onAddToFavorites} openModal={toggleOpenModal}/>
                ))}

                {modal && (                 
                    <div className="Modal" onClick={toggleOpenModal}>
                        <Modal header="Do you want to add this product to a cart?" text="Adding a product to a cart doesn't mean its reservation. Do you want to continue?" actions={<><Button text='Ok' onClick={handleAddToCart}/><Button text="Cancel" onClick={toggleOpenModal} /></>} />
                    </div>
                )}
            </div>
        </main>
    )
}

ProductList.propTypes = {
    products: PropTypes.array,
    onAddToCart: PropTypes.func,
    onAddToFavorites: PropTypes.func
}

export default ProductList;