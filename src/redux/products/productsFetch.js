import axios from "axios"
import { fetchProductsRequestAC, fetchProductsSuccessAC, fetchProductsErrorAC } from "./productsActions";

// export const fetchProducts = () => async (dispatch) => {
//     dispatch(fetchProductsRequestAC())
//     try {
//         const response = await axios.get('products.json')
//         dispatch(fetchProductsSuccessAC(response.data))
//     } catch (error) {
//         dispatch(fetchProductsErrorAC(error.message))
//     }
// }

export const fetchProducts = (url) => {
    return (dispatch) => {
        dispatch(fetchProductsRequestAC())
        axios.get(url)
        .then(response => dispatch(fetchProductsSuccessAC(response.data)))
        .catch(error => dispatch(fetchProductsErrorAC(error.message)))
    }
}