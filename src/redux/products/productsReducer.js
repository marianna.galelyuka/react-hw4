import { FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS } from './productsTypes';

const initialProductsState = {
    products: [],
    loading: false,
    error: null,
};

const productsReducer = (state = initialProductsState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_REQUEST:
            return {
                ...state,
                loading: true,
                error: null
            }
        
        case FETCH_PRODUCTS_SUCCESS:
            return {
                ...state,
                loading: false,
                // if we need to add new products we use this:
                // products: [...state.products, ...action.payload]}

                // if we need to replace existing data with the new one received from the server:
                products: action.payload,
            }

        case FETCH_PRODUCTS_ERROR:
            return {
                ...state,
                products: [],
                loading: false,
                error: action.payload
            }

        default:
            return state
        }
};

export default productsReducer;