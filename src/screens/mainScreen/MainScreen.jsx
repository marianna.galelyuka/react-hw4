import React from 'react';
import ProductList from '../../components/productList';

const MainScreen = ({ onAddToCart, onAddToFavorites }) => {
	return (
        <ProductList onAddToCart={onAddToCart} onAddToFavorites={onAddToFavorites}/>
	);
};

export default MainScreen;