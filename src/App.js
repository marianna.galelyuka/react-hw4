import React, { useState, useEffect } from "react";
import {
  createBrowserRouter,
  Route, RouterProvider, createRoutesFromElements,
} from "react-router-dom";
import { useDispatch } from "react-redux";

import Layout from "./screens/layout";
import MainScreen from "./screens/mainScreen/MainScreen";
import CartScreen from "./screens/cartScreen";
import FavoritesScreen from "./screens/favoritesScreen";
import { fetchProducts } from "./redux/products/productsFetch";

import './App.css';

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts('products.json'))
  }, [dispatch])

  const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
  const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);

  useEffect(() => {
    localStorage.setItem('favorites', JSON.stringify(favorites));
    localStorage.setItem('cart', JSON.stringify(cart));
  });

  const addToCart = (product) => {
    let isInCart = false;
    cart.forEach((item) => {
      if (item.id === product.id) {
        isInCart = true;
      }
    });

    if (!isInCart) {
      setCart([...cart, product])
    };
  }

  const addToFavorites = (product) => {
    (product.isFavorite) ? setFavorites([...favorites, product]) : deleteFromFavorites(product)
  }

  const deleteFromFavorites = (product) => {
    setFavorites(favorites.filter((item) => item.id !== product.id) );
  }

  const deleteFromCart = (id) => {
    setCart(cart.filter((item) => item.id !== id) );
  }

  const clearCart = () => {
    setCart([])
  }

  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route element={<Layout cart={cart} favorites={favorites} />}>
        <Route path="/" element={<MainScreen onAddToCart={addToCart} onAddToFavorites={addToFavorites} />} />
        <Route path="/cart" element={<CartScreen cartItems={cart} onDeleteFromCart={deleteFromCart} onClearCart={clearCart} />} />
        <Route path="/favorites" element={<FavoritesScreen favorites={favorites} />} />
      </Route>
    )
  );

  return (
    <RouterProvider router={router} />
  );
}

export default App;